var firebase = require("firebase-admin");
firebase.initializeApp({
  credential: firebase.credential.cert("firebase-key.json"),
  databaseURL: "https://debrief-v2.firebaseio.com"
});
var ref = firebase.database().ref('/');


const Language = require('@google-cloud/language');

const language = Language.v1beta2({
  projectId: "photo-1041",
  keyFilename: "keyfile.json"
});

var text = "The Trump administration on Tuesday formally announced the end of DACA -- a program that had protected nearly 800,000 young undocumented immigrants brought to the US as children from deportation. The Department of Homeland Security will stop processing any new applications for the program as of Tuesday and rescinded the Obama administration policy, Deferred Action for Childhood Arrivals. I am here today to announce that the program known as DACA that was effectuated under the Obama administration is being rescinded, Attorney General Jeff Sessions said Tuesday at the Justice Department."+
" In the five years since DACA was enacted, the nearly 800,000 individuals who have received the protections have started families, pursued careers and studied in schools and universities across the United States. The business and education communities at large have joined Democrats and many moderate Republicans in supporting the program, citing the contributions to society from the population and the sympathetic fact that many Dreamers have never known another home than the US. In a statement after his agencies and attorney general announced the decision, President Donald Trump blamed former President Barack Obama for creating the program through executive authority and urged Congress to come up with a solution. It is now time for Congress to act! he said."+
"Trump said that winding down the program would be more considerate than letting the courts end it but emphasized he stands by his America First agenda."+
"As I've said before, we will resolve the DACA issue with heart and compassion -- but through the lawful Democratic process -- while at the same time ensuring that any immigration reform we adopt provides enduring benefits for the American citizens we were elected to serve, Trump said. We must also have heart and compassion for unemployed, struggling and forgotten Americans."+
"Later, Trump told reporters he feels compassion for those affected, but long term it's going to be the right solution."+
"I have a great heart for these folks we're talking about. A great love for them and people think in terms of children but they're really young adults. I have a love for these people and hopefully now Congress will be able to help them and do it properly, he said ahead of a meeting on tax reform."+
"The administration also announced a plan to continue renewing permits for anyone whose status expires in the next six months, giving Congress time to act before any currently protected individuals lose their ability to work, study and live without fear in the US.";


var doc = {
    "type" : 'PLAIN_TEXT',
    //"content" : "Trump bad Trump Trump good bad"
    "content" : text
};

/*language.analyzeEntities({
    "document" : doc
}).then((results) => {
    var ent = results[0].entities;
    ent.forEach((e) => {
        console.log(e);
    });
});*/

language.analyzeEntitySentiment({ document: doc })
    .then((results) => {
      var entities = results[0].entities;

      console.log('Entities:');
      entities.forEach((entity) => {
        if((entity.sentiment.score>=0.40||entity.sentiment.score<=-0.40)&&(entity.type=='PERSON'||entity.type=='LOCATION'||entity.type=='ORGANIZATION'||entity.type=='EVENT')){
        console.log(`  Name: ${entity.name}`);
        console.log(`  Type: ${entity.type}`);
        console.log(`  Score: ${entity.sentiment.score}`);
        console.log(`  Magnitude: ${entity.sentiment.magnitude}`);
          
        }
      });
    })
    .catch((err) => {
      console.error('ERROR:', err);
    }
    );

